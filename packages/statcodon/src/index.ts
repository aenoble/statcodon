import {
  copyFileSync,
  existsSync,
  mkdirSync,
  readFileSync,
  statSync,
  writeFileSync,
} from "fs";
import { extname } from "path";
import minimatch from "minimatch";
import { find, logger, resolve, touch } from "./util";
import config from "./config";
import template from "./templates";

if (config.require) global.require = <NodeRequire>((id: string) => {
    if (id.startsWith("./"))
      id = `${process.cwd()}/${config.templateDir}${id.slice(1)}`;
    return require(id);
  });
else global.require = <NodeRequire>undefined;

if (!existsSync(config.outDir)) mkdirSync(config.outDir);

find(config.srcDir, { type: "d" }).forEach((d) => {
  logger.currentFilename = d;
  const newdir = `${config.outDir}/${d.split("/").slice(1).join("/")}`;
  if (!existsSync(newdir)) {
    logger.debug(`Creating directory`);
    mkdirSync(newdir);
  }
});

logger.currentFilename = "";

let lockfile = 0;
if (process.argv[2] != "-f" && existsSync(`${config.outDir}/.files`))
  lockfile = statSync(`${config.outDir}/.files`).mtimeMs;

let files = find(config.srcDir, { type: "f" });

if (lockfile != 0)
  files = files.filter((f) => statSync(f).mtimeMs > lockfile);

if (files.length == 0) {
  logger.warn(
    "Nothing to do.\n" +
      "Changed config or templates? To override, use -f option."
  );
  process.exit(0);
}

(async () => {
  for (const f of files) {
    logger.currentFilename = f;

    let skip = false;
    for (const p of config.ignore) {
      if (minimatch(f, p)) {
        skip = true;
        break;
      }
    }
    if (skip) {
      logger.debug("Skipped file");
      continue;
    }

    const ext = extname(f).slice(1);
    const name = f.slice(config.srcDir.length + 1, -(ext.length + 1));

    if (!config.formats.hasOwnProperty(ext)) {
      logger.debug("Copying file");
      copyFileSync(
        f,
        `${config.outDir}/${f.slice(config.srcDir.length + 1)}`
      );
      continue;
    }

    logger.debug("Compiling file");

    const format = config.formats[ext];
    const compiled = await resolve(
      format.compile(
        readFileSync(f, format.binary ? null : "utf8"),
        name
      )
    );

    if (format.page) {
      compiled.text = template.get(compiled.template || "default")({
        html: compiled.text.toString(),
        title: compiled.templateOptions.title || name,
        ...(compiled.templateOptions || {}),
      });

      if (config.minify)
        compiled.text = await resolve(
          config.formats.html.minify(compiled.text)
        );
    } else if (config.minify)
      compiled.text = await resolve(format.minify(compiled.text));
    writeFileSync(
      `${config.outDir}/${name}.${format.outExt}`,
      compiled.text
    );
  }
})()
  .then(() => touch(`${config.outDir}/.files`))
  .then(
    () =>
      config.hooks &&
      typeof config.hooks.finish == "function" &&
      config.hooks.finish()
  );
