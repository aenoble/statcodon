type Resolution<T> = T | Promise<T>;

export interface FormatHandler {
  compile(
    text: string | Buffer,
    filename: string
  ): Resolution<{
    text: string | Buffer;
    template: string;
    templateOptions: any & {};
  }>;
  minify?(text: string | Buffer): Resolution<string | Buffer>;
  page: boolean;
  binary?: boolean;
  outExt: string;
}
