import { compile, compileTemplate } from "pug";
import { existsSync, readFileSync } from "fs";
import config from "./config";
import { logger } from "./util";

export default new (class TemplateManager {
  private templates: Record<string, compileTemplate> = {};

  public get(name: string): compileTemplate {
    if (!this.templates[name]) {
      logger.debug(`Loading template ${name}`);
      let templateFile = `${config.templateDir}/${name}.pug`;
      if (!existsSync(templateFile)) {
        logger.warn(`Template ${name} not found. Using default.`);
        if (name != "default") return this.get("default");
        else templateFile = `${__dirname}/../page.pug`;
      }
      this.templates[name] = compile(
        readFileSync(templateFile, "utf8"),
        {
          filename: templateFile,
          basedir: process.cwd(),
          globals: ["require"],
        }
      );
    }
    return this.templates[name];
  }
})();
