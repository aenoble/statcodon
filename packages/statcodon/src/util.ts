import { closeSync, openSync, readdirSync, statSync } from "fs";
import { join } from "path";

export const identity = (t) => t;

export const touch = (f) => closeSync(openSync(f, "w"));

export async function resolve<T>(x: T | Promise<T>) {
  if (
    typeof x === "object" &&
    typeof (<Promise<T>>x).then === "function"
  )
    return await x;
  else return x;
}

export function find(
  dirname: string,
  o: {
    type?: "f" | "d";
    newer?: string;
    array?: string[];
  } = {}
) {
  let tdir = false,
    tfile = false;
  switch (o.type) {
    case "f":
      tfile = true;
      break;
    case "d":
      tdir = true;
      break;
    default:
      tdir = true;
      tfile = true;
      break;
  }

  const files = readdirSync(dirname);
  const array = o.array ? o.array : [];

  files.forEach((file) => {
    const path = join(dirname, file);
    if (statSync(path).isDirectory()) {
      if (tdir) array.push(path);
      array.push(...find(path, o));
    } else if (tfile) array.push(path);
  });

  return array;
}

export const logger = new (class Logger {
  public set currentFilename(name: string) {
    this.front = name ? name + ": " : "";
  }
  public front = "";

  public error(error: string): void {
    console.error(`error ${this.front}${error}`);
  }
  public warn(error: string): void {
    console.warn(`warn ${this.front}${error}`);
  }
  public debug(error: string): void {
    console.log(`${this.front}${error}`);
  }
})();
