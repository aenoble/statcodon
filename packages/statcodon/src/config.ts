import { existsSync } from "fs";
import { FormatHandler } from "./types";

const config: {
  srcDir: string;
  outDir: string;
  templateDir: string;
  minify: boolean;
  require: boolean;
  formats: Record<string, FormatHandler>;
  hooks: Record<string, Function>;
  ignore: string[];
} = {
  srcDir: "src",
  outDir: "dist",
  templateDir: "templates",
  minify: true,
  require: false,
  formats: {},
  hooks: {},
  ignore: [],
};

let scjs = {};
switch (true) {
  case existsSync(
    "node_modules/@statcodon/config-1.99/statcodon.config.js"
  ):
    scjs = require(`${process.cwd()}/node_modules/@statcodon/config-1.1.1/statcodon.config.js`);
    break;
  case existsSync("statcodon.config.js"):
    scjs = require(`${process.cwd()}/statcodon.config.js`);
    break;
}
Object.assign(config, scjs);

const blankCF: FormatHandler["compile"] = (text) => ({
  text,
  template: undefined,
  templateOptions: {},
});
if (!config.formats.html)
  config.formats.html = {
    compile: blankCF,
    minify: require("html-minifier").minify,
    page: false,
    outExt: "html",
  };

if (!config.formats.css) {
  const css = new (require("clean-css"))({});
  config.formats.css = <FormatHandler>{
    compile: blankCF,
    minify: (t) => css.minify(t).styles,
    page: false,
    outExt: "css",
  };
}

export default config;
