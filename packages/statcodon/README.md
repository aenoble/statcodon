# statcodon

Yet another static site generator. A start codon for your static
website.

```
npm install statcodon
pnpm install statcodon
yarn add statcodon
```

## Configuration

Configuration is done in a file named `statcodon.config.js` in the
directory it is called in. It is a CommonJS-module, so use
`module.exports =`.

### Options

- `minify` is whether to minify output files (default `true`)
- `require` is whether to give the Node.js require() as a template local
  (default `false`)
- `srcDir` is the directory to take files from (default `src`)
- `outDir` is where to put them once compiled (default `dist`)
- `templateDir` is where to find the template for pages (default
  `templates`)
- `formats` is format handlers (next section) (default `{}`)
- `hooks` is a list of functions to execute on particular events. The
  only one currently defined is `finish`. (see below) (default `{}`)
- `ignore` is shell glob strings for files to ignore. (see
  [minimatch](https://npmjs.com/package/minimatch)) (default `[]`)

### Format handlers

statcodon takes an object `formats` with keys representing file
extensions and values which are objects in the form:

```ts
interface FormatHandler {
  // Called to compile the file
  compile(
    text: string | Buffer,
    filename: string
  ): {
    text: string | Buffer;
    template: string;
    templateOptions: any & {};
  };

  // If not a "page," called to minify the compiled file
  minify?(text: string | Buffer): string | Buffer;

  // Whether to use templates and output as HTML
  page: boolean;

  // If true, compile is called with a buffer from fs.readFile instead
  // of a string.
  // If a "page," buffers will be coerced to strings (.toString())
  binary?: boolean;

  // What extension to save with
  outExt: string;
}
```

`compile` and `minify` can also be async i.e. return Promises.

Only HTML and CSS are defined by default, which only provide minifying
functions. These can be overridden by simply defining them yourself. The
default HTML is not considered a "page."

See [@statcodon/markdown](https://npmjs.com/package/@statcodon/markdown)
which provides a function to create a Markdown handler. Or write your
own, it's not that hard.

### Hooks

The `finish` hook is executed after all files are compiled. If there
were no changes, the process exits before this is run.

### Example config

```js
const css = new (require('clean-css'))({});

const config = {
  srcDir: "src",
  outDir: "dist",
  templateDir: "templates",
  formats: {
    md: require("@statcodon/markdown")({
      MarkdownIt: new MarkdownIt(),
      pandocMeta: true,
    }),
    css: {
      compile: (text) => ({text}),
      minify: (t) => css.minify(t).styles,
      page: false,
      outExt: 'css',
      binary: false,
    };
  },
};

module.exports = config;
```

## Page templating

statcodon uses [Pug](https://pugjs.org/api/getting-started.html) for
templating. In the configured template directory you can add pug
templates (extension `.pug`).

The template option named `template` determines which template is used
for a page. If it is not present, statcodon uses `default` or the
barebones `page.pug` in the package directory.
