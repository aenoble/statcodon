/** @file Copies code for statcodon */

const fs = require("node:fs");
const path = require("node:path");

function copyDir(src, dest) {
  fs.mkdirSync(dest);
  const entries = fs.readdirSync(src, { withFileTypes: true });

  for (const entry of entries) {
    let srcPath = path.join(src, entry.name);
    let destPath = path.join(dest, entry.name);

    entry.isDirectory()
      ? copyDir(srcPath, destPath)
      : fs.copyFileSync(srcPath, destPath);
  }
}

if (!fs.existsSync(__dirname + "/../statcodon/dist")) throw new Error("statcodon main is not built, cannot build shim 1.99")

try { fs.rmSync(__dirname + "/bin",  { recursive: true }); } catch (e) { console.error(e.message); }
try { fs.rmSync(__dirname + "/src",  { recursive: true }); } catch (e) { console.error(e.message); }
try { fs.rmSync(__dirname + "/dist", { recursive: true }); } catch (e) { console.error(e.message); }

copyDir(__dirname + "/../statcodon/bin",  __dirname + "/bin");
copyDir(__dirname + "/../statcodon/src",  __dirname + "/src");
copyDir(__dirname + "/../statcodon/dist", __dirname + "/dist");

fs.copyFileSync(__dirname + "/../statcodon/LICENSE",  __dirname + "/LICENSE")
fs.copyFileSync(__dirname + "/../statcodon/page.pug", __dirname + "/page.pug")
fs.writeFileSync(__dirname + "/.npmignore", "build.js\nJustfile\n.gitignore\n");

const package = JSON.parse(
  fs.readFileSync(__dirname + "/../statcodon/package.json")
);
package.version = "1.99." + package.version.slice(2).split(".").join("-");
package.repository = "https://codeberg.org/aenoble/statcodon/src/branch/master/packages/statcodon@1.99"
package.dependencies["@statcodon/config-1.99"] = "^1.0.0";
fs.writeFileSync(__dirname + "/package.json", JSON.stringify(package), () => {});
