# Stat Codon

Stat codon: a start codon for a static website.

> ## This version is deprecated
>
> statcodon 2.0.0 was released February 27, 2022. Versions 1.99.x-x are
> compatibility layers based on 2.x.x.
>
> This documentation may be outdated. See the README for
> [@statcodon/config-1.99](https://npmjs.com/package/@statcodon/config-1.99).

## Installation

Take your pick of package manager and install:

```
npm install statcodon@^1.99.0
pnpm install statcodon@^1.99.0
yarn add statcodon@^1.99.0
```

If you want you can ad a build script to `package.json`:

```jsonc
{
  // ...
  "scripts": {
    "build": "npx statcodon"
  }
  // ...
}
```

## Usage

Place files in the source directory to render. They will be output to
the dist directory. (Directories configurable.)

### Markdown

Markdown (`.md`, `.markdown`) is CommonMark with optional extensions
from markdown-it.

Heads, headers, and footers are injected into the top and bottom.

After all manipulation is complete, the resulting HTML is minified.

### HTML, Pug

Both HTML (`.htm`, `.html`) and [Pug](https://pugjs.org) (`.pug`) add
headers and footers, and are then minified.

Pug is compiled to HTML without any substitutions. Imports are parsed by
the Pug compiler, and thus do not have the same results as Markdown
imports.

### CSS, Sass

CSS (`.css`) is only minified. [Sass](https://sass-lang.com) (`.sass`)
is compiled to CSS (`@import` goes from the source directory), then
minified.

## Force recompile

To recompile all files (e.g. if you change an include's code) then set
the environment variable FORCE to a truthy value:

```sh
FORCE=1 npx statcodon
```

## Configuration

In the root of your project (same directory as `package.json`) add a
`config.json`.

The default configuration is:

```jsonc
{
  "name": "Document", // the name of your site
  "src": "src", // the directory your source files are in
  "dist": "dist", // the directory to output them to
  "ignore": [], // glob strings for files not to compile
  // automatically adds includes' files
  "minify": true, // whether to minify pages, CSS, etc
  "markdown": {
    // markdown-it configuration
    "breaks": false,
    "linkify": true,
    "typographer": true,
    "quotes": "“”‘’",
    "html": true // cannot be disabled
  },
  "markdownPlugins": [] // markdown-it plugin package names (no options yet)
}
```

**Don't use `// comments` if you change config!**

### Templating

You can template pages with `page.pug` in that directory too. You get:

- `name`: site name in `config.json`
- `includes`: head, header, footer, and other includes' HTML
- `content`: the compiled HTML of a page

In future there might be more like `title`. Minimum possible example in
`page.pug` in _this_ directory.

## Bugs

Beware of, and report.

- `transforms.ts` is a messy solution for changing text, and should
  instead be a markdown-it plugin.
- csv-to-markdown-table's type definitions suck.

## License

This project is licensed under the GNU General Public License, version
3.0 (text in `LICENSE`).
