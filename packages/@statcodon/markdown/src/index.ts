import YAML from 'yaml';
import MarkdownIt from 'markdown-it';

/**
 * Markdown support for statcodon
 *
 * @param options
 * @param options.MarkdownIt A MarkdownIt instance
 * @param options.pandocMeta Whether to enable Pandoc-like metadata blocks
 *
 * @see {@link https://github.com/mb21/markdown-it-pandoc markdown-it-pandoc} for full Pandoc markdown
 */
module.exports = function (options: {
  MarkdownIt: MarkdownIt;
  pandocMeta: boolean;
}) {
  const markdown: any = {
    _MarkdownIt: options.MarkdownIt,
    compile: mc,
    page: true,
    outExt: 'html',
  };
  function mc(text, template?, templateOptions?) {
    return {
      text: markdown._MarkdownIt.render(text),
      template: template || 'default',
      templateOptions: templateOptions || {},
    };
  }
  // Pandoc metadata block
  if (options.pandocMeta)
    markdown.compile = function (text) {
      let template,
        templateOptions: { template? } = {};
      // Pandoc metadata block
      if (text.startsWith('---')) {
        const split = text.split('---');
        if (split.length >= 3) {
          templateOptions = YAML.parse(split[1]);
          template = templateOptions.template;
        }
        text = split.slice(2).join('---');
      }
      return mc(text, template, templateOptions);
    };

  return markdown;
};
