# @statcodon/markdown

Markdown support for statcodon

```
npm install @statcodon/markdown
pnpm install @statcodon/markdown
yarn add @statcodon/markdown
```

In your config, add the file extension for Markdown (usually `.md`) and
add a call to this package. The only argument is an object, see below.

```js
{
  // ...
  formats: {
    md: require('@statcodon/markdown')({
      MarkdownIt: new MarkdownIt(),
      pandocMeta: false,
    });
  }
}
```

`pandocMeta` is whether to use Pandoc-like metadata blocks, which are
YAML at the top of the document. The template to use is determined from
the key `template`, and other than that the keys are determined by your
templates.

If you don't have `pandocMeta` enabled, you can't set template options,
and all of your pages will be the `default` template.

```
---
title: This is a title
authors:
- John Doe
- Ada Lovelace
template: default
---
```

See [markdown-it-pandoc](https://github.com/mb21/markdown-it-pandoc) for
full Pandoc markdown.

The example above produces this format handler:

```js
{
  _MarkdownIt: new MarkdownIt(),
  compile: function mc(text) {
    return {
      text: markdown._MarkdownIt.render(text),
      template: 'default',
      templateOptions: {},
    };
  },
  page: true,
  outExt: 'html'
}
```
