# @statcodon/config-1.99

- The `content` value is changed to `html` as of statcodon 2.0.0.
- `ignore` is **supported** as of statcodon 2.3.0/1.99.3-0.
- `FORCE=1` is **supported** as of this package version 1.1.0. In
  2.0.0+, use the `-f` option.
- markdown imports are **supported** as of this package version 1.1.0.
  - CSV/TSV imports are **supported** as of this package version 1.1.0.

## License

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the license, or (at your
option) any later version. Any code found in Stat Codon 1.0.0 through
1.2.0 is licensed under the Lesser General Public License, version 3.0
or later.

See <https://www.gnu.org/licenses/>.
