const package = '@statcodon/config-1.99';
const { existsSync, readFileSync } = require('node:fs');
const { extname } = require('node:path');
// FIXME: Better library than this...
const csvToMarkdown = require('csv-to-markdown-table/lib/CsvToMarkdown');

if (process.env.FORCE) process.argv[2] = '-f';

const config99 = {
  name: 'Document',
  src: 'src',
  dist: 'dist',
  includes: {
    head: '_head.html',
    header: '_header.html',
    footer: '_footer.html',
  },
  /**
   * Glob strings for  paths not to compile. Use `i.ignorePaths`
   * instead.
   */
  ignore: [],
  minify: true,
  markdown: {
    // markdown-it configuration
    breaks: false,
    linkify: true,
    typographer: true,
    quotes: '“”‘’',
    html: true, // cannot be disabled
  },
  markdownPlugins: [],
  ignorePaths: [],
};
const file99 = JSON.parse(
  existsSync('config.json') ? readFileSync('config.json') : '{}'
);
Object.assign(config99, file99);

console.log(`${package}: parsed config`);

const config = {
  srcDir: config99.src,
  outDir: config99.dist,
  formats: {},
  ignore: config99.ignore,
};

const css = new (require('clean-css'))();
config.formats.css = {
  compile: (text) => ({ text }),
  minify: (t) => css.minify(t).styles,
  outExt: 'css',
};

const sass = require('sass');
config.formats.sass = {
  compile: (t) => ({
    // FIXME: sass.renderSync is deprecated
    text: sass
      .renderSync({ data: t, includePaths: [config99.src] })
      .css.toString('utf8'),
  }),
  minify: config.formats.minify,
  outExt: 'css',
};

const { minify } = require('html-minifier');
config.formats.html = {
  compile: (text) => ({ text }),
  minify: (t) =>
    minify(t, { collapseWhitespace: true, minifyCSS: true }),
  outExt: 'html',
};
config.formats.htm = config.formats.html;

let md = new (require('markdown-it'))(config99.markdown);
config99.markdownPlugins.forEach((pl, i) => {
  if (!pl)
    throw new Error(
      `${package}: formats: markdown plugins: falsy value at index ${i}`
    );
  const pkgname = typeof pl == 'string' ? pl : pl.package || '';
  if (!pkgname)
    throw new Error(
      `${package}: formats: markdown plugins: no package name at index ${i}`
    );
  try {
    require.resolve(pkgname);
  } catch (e) {
    throw new Error(
      `${package}: formats: markdown plugins: failed to resolve\`${pkgname}'`
    );
  }
  try {
    md = md.use(require(pkgname), pl.options || {});
    console.log(
      `${package}: formats: markdown plugins: added \`${pkgname}'`
    );
  } catch (e) {
    throw new Error(
      `${package}: formats: markdown plugins: markdown-it error adding \`${pkgname}':\n\n${JSON.stringify(
        e
      )}`
    );
  }
});
const TLDs = require('tlds');
md.linkify.tlds(TLDs).add('ftp:', null);
// Imports
function markdownTransform(text) {
  const lines = [];
  text.split('\n').forEach((l, i) => {
    if (l.startsWith('$#import(') && l.endsWith(')')) {
      const file = l.slice(9).slice(0, -1);
      console.log(`${package}: imports: trying to import \`${file}'`);
      const content = existsSync(file)
        ? readFileSync(file, 'utf8')
        : '';
      const ext = extname(file).slice(1);

      let del = ',';
      switch (ext) {
        case 'tsv':
          del = '	';
        // no break
        case 'csv':
          lines[i] = csvToMarkdown(content, del, true);
          break;
        default:
          const fmt = config.formats[ext];
          if (!fmt) lines[i] = content;
          else {
            lines[i] = fmt.compilePure
              ? fmt.compilePure(content)
              : fmt.compile(content);
          }
      }
    } else lines[i] = l;
  });
}
config.formats.markdown = {
  compile: (t) => md.render(markdownTransform(t)),
  compilePure: (t) => md.render(t),
  minify: config.formats.html.minify,
  outExt: config.formats.html.outExt,
};
config.formats.md = config.formats.markdown;

const { compile } = require('pug');
config.formats.pug = {
  compile: (t) => compile(t)(),
  minify: config.formats.html.minify,
  outExt: config.formats.html.outExt,
};

console.log(
  `${package}: formats: found ${Object.keys(config.formats).join(', ')}`
);
console.log(`${package}: converted config`);

module.exports = config;
