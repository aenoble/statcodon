#!/bin/just -f

build: clean
  npx tsc

clean:
  rm -rf dist

prettify:
  npx prettier -w .

publish: prettify clean build
  yarn publish
