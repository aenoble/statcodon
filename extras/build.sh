#!/bin/bash

# Pandoc build script I made one day. Horribly inefficient, but still cool.
# Also summons Node.js and Python processes which make it even slower.

[ -d dist ] || mkdir dist

cutdir() {
	node -e 'console.log(process.argv[1].split("/").slice(1).join("/"))' "$1"
}

splitext() {
	python -c 'from os.path import splitext; from sys import argv; t=splitext(argv[1]); print(t[1])' "$1"
}

find src/ -type d |
	tail -n +2 |
	while read dir
	do
		c=$(cutdir "$dir")
		mkdir "dist/$c"
	done

find src/ -type f |
	while read file
	do
		cut=$(cutdir "$file")
		ext=$(splitext "$cut")
		fn=$(basename "$cut" "$ext")

		if test "$ext" = .md
		then pandoc --standalone --template=template.pandoc "$file" -o "dist/$fn.html"
		else cp "$file" "dist/$cut"
		fi

	done
